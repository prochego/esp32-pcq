# Implementation of CRYSTALS-Kyber with a physical unclonable function on ESP32 microcontrollers

## Introduction

This project aims to implement CRYSTALS-Kyber-90S post-quantum cryptographic scheme with keypair generated with a physically unclonable function (PUF).

When utilizing static entropy for Kyber initialization, we consistently obtain a public key suitable for flash storage, while the private key can be dynamically generated as needed, ensuring it is only stored in main memory as long as necessary.

This is the primary advantage of this approach. 
Another advantage is that the private key can generated separately with considerable speedup.

A significant portion of the project was dedicated to optimization and further improvement and expansion of the original [esp32_puflip](https://github.com/staniond/esp32_puflib)

Another significant portion of this project was focused on porting the CRYSTALS-Kyber algorithm to the ESP32 microcontroller family.

## Known obstacles
- PUF
    - PUF error rate is heavily dependent on the temperature of a chip.
    The problem is that lower temperatures reduce entropy to the point when it is no longer possible to obtain the original response.
    - One way to fight this problem is to build an SRAM temperature model which maps power-down delay to chip temperature.
    - But even if we use temperature modelling, the minimal possible temperature where we were able to obtain a valid response was about three degrees Celsius

- CRYSTALS-Kyber
    - This algorithm is very computationally and memory intensive; however, vectorization is possible using specialized SIMD instructions (only on ESP32-S3)
    - It is required to change the minimal task stack size in menuconfig to around 32768 bytes to prevent stack overflow
    
## Example

- Best and required settings:
    - CPU speed: 240Mhz
    - Main task stack size: 32768 (mandatory)
    - Flash SPI mode: QIO
    - Flash SPI speed: 80Mhz
    - Compiler optimizations: -O2
    - Logging level: Debug (for debugging)

```c++
#include <kyber_puf.hpp>
#include <kem.h>
#include <esp_log.h>
#include <nvs_flash.h>
#include <bootloader_random.h>
#include <unif_hmac.h>

static const char *TAG = "KyberPUF example";

void app_main() {
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    ESP_ERROR_CHECK(err);

    //Use only if RF subsystem is not enabled
    bootloader_random_enable();
    
    //Initializes PUF library and KyberPUF memory mapping
    init_pufkyber();
    
    uint8_t *public_key = new uint8_t[KYBER_PUBLICKEYBYTES];
    uint8_t *private_key = new uint8_t[KYBER_SECRETKEYBYTES];

    if(!kyber_generate_keypair(public_key, private_key)) {
        ESP_LOGE(TAG, "Keypair generation failed");
        return;
    }

    uint8_t *ciphertext = new uint8_t[KYBER_CIPHERTEXTBYTES];
    uint8_t ss[KYBER_SSBYTES], ss1[KYBER_SSBYTES];

    //Ciphertext and shared secret from given public key
    crypto_kem_enc(ciphertext, ss, public_key);

    //Generate shared secret from received ciphertext and private key
    crypto_kem_dec(ss1, ciphertext, private_key);

    delete[] public_key;
    delete[] private_key;
    delete[] ciphertext;

    //Deinitialize PUF library and KyberPUF memory mapping
    deinit_kyberpuf();
}
```