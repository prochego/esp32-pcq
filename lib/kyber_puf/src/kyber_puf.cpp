#include <kyber_puf.hpp>
#include <indcpa.h>
#include <puflib.h>
#include <string.h>
#include "kyber_memory.hpp"
#include "kyber_internal.hpp"

const char *kyber_tag = "Kyber";
const uint8_t *public_key_mapped = nullptr;

void init_kyberpuf() {
    if(!map_public_key(&public_key_mapped)) {
        ESP_LOGE(kyber_tag, "Public key HMAC check failed. Regeneration required.");
    }

    puflib_init();
}

void deinit_kyberpuf() {
    unmap_public_key();
    public_key_mapped = nullptr;

    puflib_deinit();
}

bool kyber_generate_keypair(uint8_t public_key[KYBER_PUBLICKEYBYTES], uint8_t private_key[KYBER_SECRETKEYBYTES]) {
    //Get valid PUF response
    if(!try_get_puf_response()) [[unlikely]] {
        ESP_LOGE(kyber_tag, "PUF failure, cannot continue");
        return false;
    }

    //If public key is not mapped, generate new keypair, save it to flash and map it
    if(public_key_mapped == nullptr) [[unlikely]] {
        ESP_LOGD(kyber_tag, "Public key not mapped, generating new keypair");

        //Full keypair generation using PUF response
        generate_keypair_internal(public_key, private_key, PUF_RESPONSE, PUF_RESPONSE_LEN);
        
        //Save public key to flash
        write_public_key(public_key);

        //Map public key to memory
        //Call abort if mapping fails (should not happen)
        assert(map_public_key(&public_key_mapped));       
    } else {
        //Generate private key using PUF response
        generate_private_key_internal(private_key, PUF_RESPONSE, PUF_RESPONSE_LEN);

        //Copy public key from flash to output buffer
        memcpy(public_key, public_key_mapped, KYBER_PUBLICKEYBYTES);
    }

    //free original full PUF response
    clean_puf_response();

    //Perform the final key transformation
    final_key_transform(public_key, private_key);
    return true;
}

bool kyber_get_public_key(uint8_t public_key[KYBER_PUBLICKEYBYTES]) {
    assert(public_key_mapped != nullptr);
    
    //Copy public key from flash to output buffer
    memcpy(public_key, public_key_mapped, KYBER_PUBLICKEYBYTES);
    return true;
}

bool kyber_get_private_key(uint8_t private_key[KYBER_SECRETKEYBYTES]) {
    assert(public_key_mapped != nullptr);
    
    //Get valid PUF response
    if(!try_get_puf_response()) [[unlikely]] {
        ESP_LOGE(kyber_tag, "PUF failure, cannot continue");
        return false;
    }

    //Generate private key using PUF response
    generate_private_key_internal(private_key, PUF_RESPONSE, PUF_RESPONSE_LEN);
    
    //free original full PUF response
    clean_puf_response();

    //Perform the final key transformation
    final_key_transform(public_key_mapped, private_key);
    return true;
}