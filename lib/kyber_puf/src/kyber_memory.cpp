#include "kyber_memory.hpp"
#include <string.h>
#include <unif_hmac.h>

struct public_key_data {
    uint8_t public_key[KYBER_PUBLICKEYBYTES];
    uint8_t public_key_hmac[HMAC_LEN];
};

esp_partition_mmap_handle_t public_key_mmap_handle = 0;

/**
 * @brief Get the partition with the public key data and HMAC
 * @return pointer to the partition
*/
const esp_partition_t *get_kyberpuf_partition() {
    const esp_partition_t *partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_UNDEFINED, KYBER_PUBLIC_PARTITION);
    if(partition != nullptr && partition->size < sizeof(public_key_data)) [[unlikely]] {
        ESP_LOGD(kyber_tag, "Partition size is too small for public key data - minumum size is %d", sizeof(public_key_data));
    }

    assert(partition != nullptr && partition->size >= sizeof(public_key_data));
    
    return partition;
}

bool map_public_key(const uint8_t **public_key) {
    const public_key_data *public_key_data;
    const esp_partition_t *partition = get_kyberpuf_partition();
    ESP_ERROR_CHECK(esp_partition_mmap(partition, 0, partition->size, ESP_PARTITION_MMAP_DATA, (const void **) &public_key_data, &public_key_mmap_handle));

    if(!check_hmac(public_key_data->public_key, KYBER_PUBLICKEYBYTES, public_key_data->public_key_hmac)) {
        esp_partition_munmap(public_key_mmap_handle);
        public_key_mmap_handle = 0;

        ESP_ERROR_CHECK(esp_partition_erase_range(partition, 0, partition->size));
        return false;
    }

    *public_key = public_key_data->public_key;
    return true;
}

void unmap_public_key() {
    if(public_key_mmap_handle == 0)
        return;

    esp_partition_munmap(public_key_mmap_handle);
    public_key_mmap_handle = 0;
}

void write_public_key(const uint8_t *public_key) {
    public_key_data public_key_d;
    calculate_hmac(public_key, KYBER_PUBLICKEYBYTES, public_key_d.public_key_hmac);
    memcpy(public_key_d.public_key, public_key, KYBER_PUBLICKEYBYTES);

    const esp_partition_t *partition = get_kyberpuf_partition();

    //Assuming that the partition is erased by init operation
    ESP_ERROR_CHECK(esp_partition_write(partition, 0, &public_key_d, sizeof(public_key_data)));
}