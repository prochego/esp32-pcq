#include "kyber_internal.hpp"
#include "kyber_defines.hpp"

#include <indcpa.h>
#include <string.h>
#include <esp_random.h>
#include <puflib.h>
#include <symmetric.h>

void final_key_transform(const uint8_t *public_key, uint8_t *private_key) {
    memcpy(private_key + KYBER_INDCPA_SECRETKEYBYTES, public_key, KYBER_INDCPA_PUBLICKEYBYTES);

    hash_h(private_key + KYBER_SECRETKEYBYTES - 2 * KYBER_SYMBYTES, public_key, KYBER_PUBLICKEYBYTES);

    esp_fill_random(private_key + KYBER_SECRETKEYBYTES - KYBER_SYMBYTES, KYBER_SYMBYTES);
}

void generate_private_key_internal(uint8_t *private_key, uint8_t *puf_response, uint32_t puf_response_len) {
    uint8_t nonce = 0;
    polyvec skpv;

    hash_h(puf_response, puf_response, puf_response_len);

    for(uint8_t i = 0; i < KYBER_K; i++)
        poly_getnoise_eta1(&skpv.vec[i], puf_response, nonce++);
    
    polyvec_ntt(&skpv);
    pack_sk(private_key, &skpv);
}

void generate_keypair_internal(uint8_t *public_key, uint8_t *private_key, uint8_t *puf_response, uint32_t puf_response_len) {    
    uint8_t public_seed[2 * KYBER_SYMBYTES];
    uint8_t *noise_seed = public_seed + KYBER_SYMBYTES;
    uint8_t i, nonce = 0;
    polyvec a[KYBER_K], e, pkpv, skpv;

    esp_fill_random(public_seed, KYBER_SYMBYTES);
    hash_g(public_seed, public_seed, KYBER_SYMBYTES);

    gen_matrix(a, public_seed, 0);

    hash_h(puf_response, puf_response, puf_response_len);
    for(i = 0; i < KYBER_K; i++)
        poly_getnoise_eta1(&skpv.vec[i], puf_response, nonce++);
    
    for(i = 0; i < KYBER_K; i++)
        poly_getnoise_eta1(&e.vec[i], noise_seed, nonce++);

    polyvec_ntt(&skpv);
    polyvec_ntt(&e);

    // matrix-vector multiplication
    for(i = 0; i < KYBER_K; i++) {
        polyvec_pointwise_acc_montgomery(&pkpv.vec[i], &a[i], &skpv);
        poly_tomont(&pkpv.vec[i]);
    }

    polyvec_add(&pkpv, &pkpv, &e);
    polyvec_reduce(&pkpv);

    pack_sk(private_key, &skpv);
    pack_pk(public_key, &pkpv, public_seed);
}

bool try_get_puf_response() {
    uint8_t failed_respones = 0;
    while (failed_respones < MAX_PUF_FAILS && !get_puf_response()) {
        vTaskDelay(2);
        failed_respones++;
    }

    return failed_respones < MAX_PUF_FAILS && PUF_RESPONSE != NULL;
}