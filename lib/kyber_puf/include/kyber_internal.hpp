//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
#include <stdint.h>

/**
 * @brief Used to perform the final private key transformation. Adding the public key and its hash plus random bytes.
 * @param public_key pointer to the public key
 * @param private_key pointer to the private key
*/
void final_key_transform(const uint8_t *public_key, uint8_t *private_key);

/**
 * @brief Used to generate private key from PUF response.
 * @param private_key pointer to the private key
 * @param puf_response pointer to the PUF response
 * @param puf_response_len length of the PUF response
*/
void generate_private_key_internal(uint8_t *private_key, uint8_t *puf_response, uint32_t puf_response_len);

/**
 * @brief Used to generate public and private key from PUF response.
 * @param public_key pointer to the public key
 * @param private_key pointer to the private key
 * @param puf_response pointer to the PUF response
 * @param puf_response_len length of the PUF response
*/
void generate_keypair_internal(uint8_t *public_key, uint8_t *private_key, uint8_t *puf_response, uint32_t puf_response_len);

/**
 * @brief Used to get PUF response from the PUF library.
 * @return ``true`` on success, ``false`` on failure
 * @note This function fails after ``MAX_PUF_FAILS`` attempts.
*/
bool try_get_puf_response();