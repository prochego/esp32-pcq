//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
#include <esp_partition.h>
#include <unif_hmac.h>
#include <indcpa.h>
#include <esp_log.h>
#include "kyber_defines.hpp"

/**
 * @brief Used to map public key from KyberPUF partition.
 * @param public_key pointer to the public key
 * @return ``true`` if key was loaded successfully, otherwise ``false`` (HMAC mismatch)
*/
bool map_public_key(const uint8_t **public_key);

/**
 * @brief Used to unmap public key from KyberPUF partition.
*/
void unmap_public_key();

/**
 * @brief Used to write public key and its HMAC to KyberPUF partition.
 * @param public_key pointer to the public key
*/
void write_public_key(const uint8_t *public_key);