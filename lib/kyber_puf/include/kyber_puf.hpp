//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
#ifndef KYBER_PUF
#define KYBER_PUF

#include <kem.h>
#include <puflib.h>

/**
 * @brief Used to initialize the KyberPUF library.
 * @attention This function must be called before any other function from this library.
*/
void init_kyberpuf();

/**
 * @brief Used to deinitialize the KyberPUF library.
*/
void deinit_kyberpuf();

/**
 * @brief Generates public and private key using Kyber-512-90S algorithm
 * @param public_key preallocated array of ``KYBER_PUBLICKEYBYTES``, will contain public key
 * @param private_key preallocated array of ``KYBER_SECRETKEYBYTES``, will contain private_key
 * @return ``true`` on success, ``false`` on failure
*/
bool kyber_generate_keypair(uint8_t public_key[KYBER_PUBLICKEYBYTES], uint8_t private_key[KYBER_SECRETKEYBYTES]);

/**
 * @brief Used to obtain only public key from NVS.
 * @attention This function does not generate public key it self. Public key needs to be generate with ``kyber_generate_keypair`` function before using this function.
 * @param public_key output array of ``KYBER_PUBLICKEYBYTES`` bytes
 * @return ``true`` if key was loaded successfully, otherwise ``false`` - key does not exists in NVS or other NSV related error
*/
bool kyber_get_public_key(uint8_t public_key[KYBER_PUBLICKEYBYTES]);

/**
 * @brief Used to obtain private key generating it on fly.
 * @param private_key output array of ``KYBER_SECRETKEYBYTES`` bytes.
 * @return ``true`` on success, ``false`` on failure - PUF failure
*/
bool kyber_get_private_key(uint8_t private_key[KYBER_SECRETKEYBYTES]);

#endif //KYBER_PUF