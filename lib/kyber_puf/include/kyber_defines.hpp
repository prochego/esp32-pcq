//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
#include <params.h>

#define KYBER_PUBLIC_PARTITION "kyber_data"
#define MAX_PUF_FAILS   10

extern const char *kyber_tag;