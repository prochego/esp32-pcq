#include "sha2.h"
#include <mbedtls/sha512.h>

inline void sha512(uint8_t out[64],const uint8_t *in,size_t inlen) {
  mbedtls_sha512(in, inlen, out, 0);
}
