#ifndef KEM_H
#define KEM_H

#include "params.h"

#ifdef __cplusplus
extern "C" {
#endif

int crypto_kem_keypair(unsigned char *pk, unsigned char *sk);

int crypto_kem_enc(unsigned char *ct,
                   unsigned char *ss,
                   const unsigned char *pk);

int crypto_kem_dec(unsigned char *ss,
                   const unsigned char *ct,
                   const unsigned char *sk);

#ifdef __cplusplus
}
#endif

#endif
