#ifndef POLYVEC_H
#define POLYVEC_H

#include <stdint.h>
#include "params.h"
#include "poly.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct{
  poly vec[KYBER_K];
} polyvec;

void polyvec_compress(uint8_t r[KYBER_POLYVECCOMPRESSEDBYTES], polyvec *a);

void polyvec_decompress(polyvec *r,
                        const uint8_t a[KYBER_POLYVECCOMPRESSEDBYTES]);

void polyvec_tobytes(uint8_t r[KYBER_POLYVECBYTES], polyvec *a);

void polyvec_frombytes(polyvec *r, const uint8_t a[KYBER_POLYVECBYTES]);


void polyvec_ntt(polyvec *r);

void polyvec_invntt_tomont(polyvec *r);

void polyvec_pointwise_acc_montgomery(poly *r,
                                      const polyvec *a,
                                      const polyvec *b);

void polyvec_reduce(polyvec *r);

void polyvec_csubq(polyvec *r);


void polyvec_add(polyvec *r, const polyvec *a, const polyvec *b);

#ifdef __cplusplus
}
#endif

#endif
