#include "aes256ctr.h"
#include "hal/aes_hal.h"
#include "esp_compiler.h"
#include <mbedtls/aes.h>
#include <string.h>

void esp_aes_block(const aes256ctr_ctx *ctx, const void *input, void *output)
{
    uint32_t i0, i1, i2, i3;
    const uint32_t *input_words = (uint32_t *)input;
    uint32_t *output_words = (uint32_t *)output;

    /* If no key is written to hardware yet, either the user hasn't called
       mbedtls_aes_setkey_enc/mbedtls_aes_setkey_dec - meaning we also don't
       know which mode to use - or a fault skipped the
       key write to hardware. Treat this as a fatal error and zero the output block.
    */
    if (ctx->key_in_hardware != AES256CTR_KEYBYTES) {
        mbedtls_platform_zeroize(output, 16);
        abort();
    }
    
    i0 = input_words[0];
    i1 = input_words[1];
    i2 = input_words[2];
    i3 = input_words[3];

    aes_hal_transform_block(input, output);

    /* Physical security check: Verify the AES accelerator actually ran, and wasn't
       skipped due to external fault injection while starting the peripheral.

       Note that i0,i1,i2,i3 are copied from input buffer in case input==output.

       Bypassing this check requires at least one additional fault.
    */
    if(i0 == output_words[0] && i1 == output_words[1] && i2 == output_words[2] && i3 == output_words[3]) {
        // calling zeroing functions to narrow the
        // window for a double-fault of the abort step, here
        memset(output, 0, 16);
        mbedtls_platform_zeroize(output, 16);
        abort();
    }
}

/**
 * @brief Run AES-256-CTR pseudo random function with output of desired len
 * @param out pointer to output array
 * @param outlen desired output length in bytes
 * @param key AES primary 256bit key
 * @param nonce 12byte nonce with 4byte counter, counter MUST be initialized to zero.
*/
void aes256ctr_prf(uint8_t *out, size_t outlen, const uint8_t *key, const uint8_t *nonce) {
	aes256ctr_ctx ctx;
    
    esp_aes_acquire_hardware();
    
    aes256ctr_init(&ctx, key, nonce);

	while (outlen >= 16) {
		esp_aes_block(&ctx, ctx.nonce_counter, out);
		outlen -= 16;
		out += 16;

		for (int8_t i = 16; i > 0; i-- ) {
			if ( ++ctx.nonce_counter[i - 1] != 0 )
				break;
		}
	}

    //process last block
	if(outlen > 0) {
		uint8_t stream_block[AES256CTR_BLOCKBYTES];
		esp_aes_block(&ctx, ctx.nonce_counter, stream_block);
		memcpy(out, stream_block, outlen);
	}

	aes256ctr_free(&ctx);

    esp_aes_release_hardware();
}

/**
 * @brief Initialize AES-256-CTR context with key and nonce.
 * @attention HW must be locked prior to this operation
*/
void aes256ctr_init(aes256ctr_ctx *ctx, const uint8_t *key, const uint8_t *nonce) {
    //initialize nonce counter
    memcpy(ctx->nonce_counter, nonce, 12);
    memset(ctx->nonce_counter + 12, 0, 4);
    
    //initialize AES key directly
    ctx->key_in_hardware = 0;
    ctx->key_in_hardware = aes_hal_setkey(key, AES256CTR_KEYBYTES, ESP_AES_ENCRYPT);
}

/**
 * @brief Run AES-256-CTR for N blocks.
 * @attention AES context must be initialized and HW must be locked prior to this operation.
 * @param out pointer to ouput array
 * @param nblocks number of 16 byte blocks
 * @param ctx AES context
*/
void aes256ctr_squeezeblocks(uint8_t *out, size_t nblocks, aes256ctr_ctx *ctx) {
	while(nblocks != 0) {
		esp_aes_block(ctx, ctx->nonce_counter, out);

		for (int8_t i = 16; i > 0; i-- ) {
			if ( ++ctx->nonce_counter[i - 1] != 0 )
				break;
		}

		--nblocks;
		out += AES256CTR_BLOCKBYTES;
	}
}

inline void aes256ctr_free(aes256ctr_ctx *ctx) {
    mbedtls_platform_zeroize(ctx, sizeof(aes256ctr_ctx));
}

inline void aes256ctr_lock_hw() {
    esp_aes_acquire_hardware();
}

inline void aes256ctr_release_hw() {
    esp_aes_release_hardware();
}
