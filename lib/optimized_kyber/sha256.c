#include "sha2.h"
#include <mbedtls/sha256.h>

inline void sha256(uint8_t out[32], const uint8_t *in,size_t inlen) {
  mbedtls_sha256(in, inlen, out, 0);
}
