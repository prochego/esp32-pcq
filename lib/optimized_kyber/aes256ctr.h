#ifndef AES256CTR_H
#define AES256CTR_H

#include <stddef.h>
#include <stdint.h>

#define AES256CTR_BLOCKBYTES 16
#define AES256CTR_KEYBYTES 32

#ifdef __cplusplus
extern "C" {
#endif

typedef struct aes256ctr_ctx {
  uint8_t nonce_counter[AES256CTR_BLOCKBYTES];
  volatile uint8_t key_in_hardware;
} aes256ctr_ctx;

void aes256ctr_prf(uint8_t *out,
                   const size_t outlen,
                   const uint8_t *key,
                   const uint8_t *nonce);

void aes256ctr_init(aes256ctr_ctx *ctx,
                    const uint8_t *key,
                    const uint8_t *nonce);

void aes256ctr_squeezeblocks(uint8_t *out,
                             const size_t nblocks,
                             aes256ctr_ctx *ctx);

void aes256ctr_free(aes256ctr_ctx *ctx);


void aes256ctr_lock_hw();
void aes256ctr_release_hw();

#ifdef __cplusplus
}
#endif

#endif
