//
// Original author: 
//      Ondrej Stanicek
//      staniond@fit.cvut.cz
//      2022
//
// Modified by:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
//
// Czech Technical University - Faculty of Information Technology
#ifndef ESP32_PUF_PUFLIB_H
#define ESP32_PUF_PUFLIB_H

#include <esp_attr.h>
#include <stdbool.h>
#include <stdlib.h>

#include "puf_defines.h"

#ifdef __cplusplus
    extern "C" {
#endif

/**
 * Buffer to which the PUF response will be copied after calling the get_puf_response or get_puf_response_reset functions
 */
extern uint8_t *PUF_RESPONSE;

/**
 * @brief Used to initialize puflib. Needs to be called before other puflib operation
 * @note  If PUF provision does not exists in ``puf_partition`` enrolment is done. (RTC / deep sleep based on config)
 * @note  Aborts if PUF data integrity check fails.
 * @attention If deep sleep version is enabled and provision does not exists system is restarted ``PROVISIONING_MEASUREMENTS_SLEEP`` times. (Should be called at beginning of app_main function)
 */
void puflib_init();

/**
 * @brief Used to set PUFlib to it's initial state.
 * @note  Unmaps ``puf_partition`` and sets data pointers to NULL
*/
void puflib_deinit();

/**
 * @brief Used obtain RTC PUF response.
 * @attention If function returns ``false`` ``PUF_RESPONSE`` will be ``NULL``. In this case this function needs to be call once again.
 * @attention If valid RTC PUF response if obtained it needs to be freed using ``clean_puf_response`` otherwise memory leak will ocur.
 * @return ``true`` if valid RTC PUF response was obtained otherwise ``false`` (ECC correction failed)
 */
bool get_puf_response();

#ifdef PUF_ENABLE_DEEP_SLEEP

/**
 * Gets the PUF response and saves is to the global PUF_RESPONSE buffer as well as setting the length of the PUF
 * response in bytes to the PUF_RESPONSE_LEN global variable.
 * The PUF response needs to be cleaned using the clean_puf_response function after it has been used.
 * @attention This function will cause the chip to reset. The app needs to save all of it's state to RTC memory.
 * @return If this function returns ``false`` sleep was rejected or raw PUF data are ready 
 */
bool get_puf_response_reset();

/**
 * This function needs to be called somewhere from the deep sleep wake up stub of the esp-idf
 * (the esp_wake_deep_sleep function).
 */
void puf_sleep_wake_up_stub(void);

void puf_response_reset_calculate();

/**
 * @brief Used to check if PUF sleep response was obtained successfully.
 * @attention User must check if response is valid, otherwise there is a risk of segmentation fault.
 * @return ``true`` if PUF response is valid otherwise ``false``
*/
bool puf_sleep_data_ready();

#endif

/**
 * Cleans the PUF response by overwriting it and releases the allocated memory.
 */
void clean_puf_response();

#ifdef __cplusplus
    }
#endif

#endif //ESP32_PUF_PUFLIB_H
