//
// Original author: 
//      Ondrej Stanicek
//      staniond@fit.cvut.cz
//      2022
//
// Modified by:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
//
// Czech Technical University - Faculty of Information Technology
#ifndef ESP32_PUF_PUF_MEASUREMENT_H
#define ESP32_PUF_PUF_MEASUREMENT_H

#include "puf_defines.h"

#include <stdlib.h>
#include <esp_attr.h>
#include <esp_partition.h>
#include <mbedtls/platform_util.h>

#ifdef PUF_USE_TEMPERATURE_MODEL
#include "driver/temperature_sensor.h"
extern temperature_sensor_handle_t temp_handle;
#endif

/**
 * @brief PUF helper data structure, stores the stable bit mask and the ECC data
*/
typedef struct puf_provision_data {
    size_t begin_delim;

#ifdef PUF_ENABLE_RTC
    //RTC version ECC helper data
    uint8_t rtc_ecc_data[PUF_MEMORY_SIZE];

    //RTC version stable bit mask
    uint8_t rtc_mask_data[PUF_MEMORY_SIZE];
#endif

#ifdef PUF_ENABLE_DEEP_SLEEP
    //Deep sleep version ECC helper data
    uint8_t sleep_ecc_data[PUF_MEMORY_SIZE];

    //Deep sleep version stable bit mask
    uint8_t sleep_mask_data[PUF_MEMORY_SIZE];
#endif

    size_t end_delim;

    //HMAC of the structure (not including the HMAC itself)
    uint8_t provision_data_hmac[32];
} puf_provision_data;


extern const puf_provision_data *puf_provision;

#ifdef __cplusplus
    extern "C" {
#endif

extern uint8_t *PUF_RESPONSE;

/**
 * Generates frequency array of the PUF response.
 * puf_freq[i] is the number of times the i-th bit in the puf response was 1 during \p measurements measurements.
 * (if the bit is 1 all the time, puf_freq[i] == \p measurements)
 * @param puf_freq the resulting frequency array
 */
void get_puf_bit_frequency(uint16_t **puf_freq);

#ifdef PUF_ENABLE_DEEP_SLEEP

/**
 * Generates frequency array of the PUF response.
 * puf_freq[i] is the number of times the i-th bit in the puf response was 1 during \p measurements measurements.
 * (if the bit is 1 all the time, puf_freq[i] == \p measurements)
 * Deep sleep PUF version is used.
 * @param puf_freq the resulting frequency array will be set to this buffer
 * @param len length of the \p puf_freq (needs to be 8 * size of the PUF SRAM region read)
 */
void get_pufsleep_bit_frequency(uint16_t **puf_freq);

bool get_puf_response_reset();

void puf_sleep_wake_up_stub();

void puf_response_reset_calculate();

bool puf_sleep_data_ready();

#endif

#ifdef PUF_ENABLE_RTC

bool get_puf_response();

/**
 * @brief Only used for temperature modelling
 * @param delay SRAM power down delay in ms
 * @return ``true`` if the response was successfully reconstructed, otherwise ``false``
*/
bool get_puf_response_temp_model(const size_t delay);

#endif

void clean_puf_response();

/**
 * @brief Used to fetch partition pointer
 * @note If partition does not exists or is not right size then abort is called
 * @return constant pointer to partition structure
*/
const esp_partition_t *get_puf_partition();

#ifdef __cplusplus
    }
#endif

#endif //ESP32_PUF_PUF_MEASUREMENT_H