//
// Original author:
//      Ondrej Stanicek
//      staniond@fit.cvut.cz
//      2022
// Modified by:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
//
#ifndef ESP32_PUF_ECC_H
#define ESP32_PUF_ECC_H

#ifdef __cplusplus
    extern "C" {
#endif

/**
 * @brief Provisions the PUF on this device - saves stable bit mask and ECC data to flash for stable PUF response reconstruction.
 * @note Provisions PUF for both RTC and deep sleep methods based on configuration.
 * @attention If deep sleep version is used, this function will reset the device multiple times.
 */
void enroll_puf();

/**
 * @brief Corrects the PUF response using the ECC data. The ECC used is 8x repetition code.
 * @param masked_data Masked PUF response to be corrected
 * @param ecc_data Precalculated ECC data
 * @param result array to which the corrected PUF response is saved - function allocates array by it self
 * @return ``true`` if error correction succeeded otherwise ``false``, if ``false`` result is set to ``NULL``
 * @note Resulting array will to be ``PUF_MEMORY_SIZE / 8`` bytes long
 */
int correct_data(const uint64_t *masked_data, const uint64_t *ecc_data, uint8_t **result);

/**
 * @brief Applies the stable bit mask to the puf response
 * @param mask mask of stable bits
 * @param puf_response Measured PUF response to mask
 * @note The mask is applied in place
 */
void apply_puf_mask(const uint32_t *mask, uint32_t *puf_response);

#ifdef __cplusplus
    }
#endif

#endif //ESP32_PUF_ECC_H
