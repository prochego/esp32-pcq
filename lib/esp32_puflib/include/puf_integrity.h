//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
//
#ifndef PUF_INTEGRITY
#define PUF_INTEGRITY

#include "puf_measurement.h"
#include <unif_hmac.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Used to calculate HMAC value for provision structure
 * @note If SOC have HMAC peripheral then HW accelerated HMAC is used, otherwise this function falls back to software implementation
 * @attention If HW HMAC is used, then before operation key needs to be burned to eFuses using the eFuse manager.
 * @param data pointer to the ``puf_provision_data`` structure
 * @param out_data array of at least 32 bytes where the calculated HMAC is stored
*/
void calculate_puf_provision_hmac(const puf_provision_data *data, uint8_t out_data[HMAC_LEN]);

/**
 * @brief Used to check if saved and calculated HMAC values are equal
 * @param data pointer to the ``puf_provision_data`` structure
 * @return ``true`` if original and calculated HMAC equals, otherwise ``false``
*/
bool puf_integrity_check(const puf_provision_data *data);

#ifdef __cplusplus
}
#endif

#endif //PUF_INTEGRITY