//
// Author:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
//
#ifndef PUF_DEFINES
#define PUF_DEFINES

#include "soc/soc_caps.h"
#include "sdkconfig.h"

//Comment this out if sleep version of PUF is not required
//Saves code size
//#define PUF_ENABLE_DEEP_SLEEP

//Comment this ouf if RTC version is not available or not required
//Saves code size
#define PUF_ENABLE_RTC

#define PUFSLEEP_RESPONSE_SLEEP_uS 300000

#define PUF_ERROR_THRESHOLD_PERCENT 0.20

//Minimum number of stable bits
//This value is dependent on application, default value is half of measured RAM size
#define PUF_STABLE_BIT_THRESHOLD PUF_MEMORY_SIZE / 2 * 8

//Number of provisioning measurements for D-SRAM
//Also defines how many times will CPU reset
//And also how many flash writes will be made - keep this value as low as possible
#define PROVISIONING_MEASUREMENTS_SLEEP 10

//Number of RTC ram measurements
//This value is dependent on current SOC
//More measurements means less errors
#define PROVISIONING_MEASUREMENTS_RTC 2000

//If currently used ESP32 supports temperature measurement, one can create error-temperature model
//Using polynomial regression where 4-degree polynomial is used (max)
//Comment this out to fall back to constant waiting time
//#define PUF_USE_TEMPERATURE_MODEL

#ifdef PUF_USE_TEMPERATURE_MODEL
    #define X4_COEFF 5.15624876e-03
    #define X3_COEFF -5.06227882e-01
    #define X2_COEFF 1.61585460e+01
    #define X1_COEFF -2.38715013e+02
    #define  C_COEFF 2.20562494e+03
#else
    //This waiting time should be big enough to allow SRAM to properly discharge
    #define PUF_RESPONSE_SLEEP_uS 500
#endif

// value of probability of bit flip to declare it stable (or lower)
// 0.001 works perfectly and gives about 380 bytes of PUF response
// 0.005 seems to work also and gives about 400 bytes of PUF response
#define STABLE_BIT_PROBABILITY 0.001

//Number of measured bytes
// max is 0x2000 - 8KB for RTC FAST SRAM
#define PUF_MEMORY_SIZE 0x1000

#define PUF_RESPONSE_LEN (PUF_MEMORY_SIZE / 8)

//PUF data partition name
//Same name needs to be defined in partition table
#define PUF_PARTITION_NAME "puf_data"

//PUF data partition size in bytes
//Same size needs to defined in partition table and needs to be big enough to hold all provision data
#define PUF_PARTITION_SIZE 0x10000


//Dont change values bellow until necessary

#define PUF_ERROR_THRESHOLD (PUF_ERROR_THRESHOLD_PERCENT * (PUF_MEMORY_SIZE * 8 / (float)100))

//Size of RTC fast memory in bytes
#define RTC_FASTMEM_SIZE 0x2000

//Configuration based on ESP32 versions
#if defined CONFIG_IDF_TARGET_ESP32S3
    #define DATA_SRAM_MEMORY_ADDRESS 0x3FCF0000
#elif defined CONFIG_IDF_TARGET_ESP32S2
    #define PUF_SUPPORT_RTC
    #define RTC_FAST_MEMORY_ADDRESS  0x3FF9E000
    #define DATA_SRAM_MEMORY_ADDRESS 0x3FFB8000
#elif defined CONFIG_IDF_TARGET_ESP32C2 || defined CONFIG_IDF_TARGET_ESP8684 
    #define DATA_SRAM_MEMORY_ADDRESS 0x3FCA0000
#elif defined CONFIG_IDF_TARGET_ESP32 
    #define PUF_SUPPORT_RTC
    #define RTC_FAST_MEMORY_ADDRESS  0x3FF80000
    #define DATA_SRAM_MEMORY_ADDRESS 0x3FFB0000
#else
    #error "Unsupported ESP32 version!"
#endif

#if PUF_MEMORY_SIZE > RTC_FASTMEM_SIZE
    #error "PUF_MEMORY_SIZE cannot be greater than the RTC FAST SRAM itself!"
#endif

#ifdef PUF_ENABLE_RTC
    #ifndef PUF_SUPPORT_RTC
        //#error "RTC version of PUF is not supported"
    #endif
#endif

#ifdef PUF_USE_TEMPERATURE_MODEL
    #ifndef SOC_TEMP_SENSOR_SUPPORTED
        #error "Cant use temperature model without temperature sensor"
    #endif
#endif

extern const char *PUF_TAG;

#endif //PUF_DEFINES