//
// Original author:
//      Ondrej Stanicek
//      staniond@fit.cvut.cz
//      2022
// Modified by:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
//
#ifndef BIT_MANIP
#define BIT_MANIP

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
    extern "C" {
#endif

void SET_BIT_arr(uint8_t *value, const size_t position, const bool bit);

bool GET_BIT(const uint8_t *value, const size_t position);

bool HIGHEST_BIT(const uint8_t value);

void SET_BIT(uint8_t *value, const uint8_t position, const bool bit);

#ifdef __cplusplus
    }
#endif

#endif