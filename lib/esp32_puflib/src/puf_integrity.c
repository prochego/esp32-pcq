#include "puf_integrity.h"

void calculate_puf_provision_hmac(const puf_provision_data *data, uint8_t out_data[HMAC_LEN]) {
    calculate_hmac((const uint8_t*) data, sizeof(puf_provision_data) - HMAC_LEN, out_data);
}

bool puf_integrity_check(const puf_provision_data *data) {
    return check_hmac((const uint8_t*) data, sizeof(puf_provision_data) - HMAC_LEN, data->provision_data_hmac);
}