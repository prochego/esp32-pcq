#include <stdio.h>
#include <string.h>
#include <esp_log.h>

#include "puf_measurement.h"
#include "ecc.h"
#include "puf_defines.h"
#include "puf_integrity.h"
#include "unif_hmac.h"

const char *PUF_TAG = "PUF";

uint8_t *PUF_RESPONSE = NULL;
const puf_provision_data *puf_provision = NULL;
esp_partition_mmap_handle_t mmap_handle = 0;

#ifdef PUF_USE_TEMPERATURE_MODEL
temperature_sensor_handle_t temp_handle = 0;
#endif

/**
 * @brief Used to get pointer to PUF data partition
 * @note  Partition name is defined in ``puf_defines.h`` in ``PUF_PARTITION_NAME`` define
 * @note  Partition size is also defined in this file in ``PUF_PARTITION_SIZE`` define
 * @attention With deep sleep enabled partition must considerably bigger to be able to hold intermidiate provision data
 * @attention Partition side defined in ``puf_defines.h`` must exactly match size defined in partition table
*/
const esp_partition_t *get_puf_partition() {
    const esp_partition_t *puf_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_UNDEFINED, PUF_PARTITION_NAME);
    
    //Abort if puf_partition does not exist or its not the right size
    assert(puf_partition != NULL && puf_partition->size >= sizeof(puf_provision_data));

    return puf_partition;
}

/**
 * @brief Used to check if PUF provision data exist on flash
 * @attention This function only does very basic provision existence test.
 * @return true if puf_provision_data start and end delimiters were found at designated positions. NOTHING ELSE
*/
bool provision_exists() {
    size_t begin, end;
    const esp_partition_t *puf_data_partition = get_puf_partition();
    
    //Load begin delimiter
    ESP_ERROR_CHECK(esp_partition_read(puf_data_partition, offsetof(puf_provision_data, begin_delim), (void*) &begin, sizeof(size_t)));
    
    //Load end delimiter
    ESP_ERROR_CHECK(esp_partition_read(puf_data_partition, offsetof(puf_provision_data, end_delim),   (void*) &end, sizeof(size_t)));    

    //Begin delimited must be equal 0xAF and end delimiter must be equal 0xFA
    return begin == 0xAF && end == 0xFA;
}

/**
 * @brief Used to memory map ``puf_partition``
 * @note  This function does HMAC integrity check on ECC and mask data
 * @note  If integrity check fails ``puf_partition`` is erased
 * @attention See attention in ``provision_integrity_verify``
 * @return ``true`` if memory mapping and integrity checks are OK otherwise ``false``
*/
bool map_data_partition() {
    const puf_provision_data *provision_data;
    const esp_partition_t *puf_data_partition = get_puf_partition();
    ESP_ERROR_CHECK(esp_partition_mmap(puf_data_partition, 0, PUF_PARTITION_SIZE, ESP_PARTITION_MMAP_DATA, (const void**) &provision_data, &mmap_handle));

    const bool puf_provision_integrity = puf_integrity_check(provision_data);
    if(!puf_provision_integrity) {
        //If integrity check fails we want to erase all provision data because they were potentially tempered with
        ESP_ERROR_CHECK(esp_partition_erase_range(puf_data_partition, 0, PUF_PARTITION_SIZE));
        ESP_LOGE(PUF_TAG, "PUF provision integrity check failed");
        return false;
    }
    
    puf_provision = provision_data;
    return true;
}

void puflib_init() {
//Enable temperature measuring if supported and enabled
//We want to do this before provisioning
#ifdef PUF_USE_TEMPERATURE_MODEL
    const temperature_sensor_config_t temp_config = TEMPERATURE_SENSOR_CONFIG_DEFAULT(-10, 80);
    ESP_ERROR_CHECK(temperature_sensor_install(&temp_config, &temp_handle));
    ESP_ERROR_CHECK(temperature_sensor_enable(temp_handle));
#endif

    //Check if provision exists and if not enroll PUF
    if(!provision_exists()) {
        ESP_LOGD(PUF_TAG, "enrolling...");
        enroll_puf();
        ESP_LOGD(PUF_TAG, "enrolment done");
    }

    assert(map_data_partition());

    ESP_LOGD(PUF_TAG, "Initialization done");
}

void puflib_deinit() {
    //Try to clean PUF response (just in case)
    clean_puf_response();

    //Unmap partition mapped in puflib_init()
    esp_partition_munmap(mmap_handle);
    mmap_handle = 0;

    puf_provision = NULL;

//Also disable and uninstall temperature sensor if SOC supports it
#ifdef PUF_USE_TEMPERATURE_MODEL
    temperature_sensor_disable(temp_handle);
    temperature_sensor_uninstall(temp_handle);
    temp_handle = 0;
#endif
}

void clean_puf_response() {
    //There is nothing to erase
    if(PUF_RESPONSE == NULL)
        return;

    //Securely zeroize PUF_RESPONSE buffer
    mbedtls_platform_zeroize(PUF_RESPONSE, PUF_RESPONSE_LEN);

    free(PUF_RESPONSE);
    
    //Reset data values to initial states
    PUF_RESPONSE = NULL;
}