#include "puf_defines.h"

#ifdef PUF_ENABLE_DEEP_SLEEP

#include "puf_measurement.h"
#include "puf_defines.h"
#include "bit_manip.h"
#include "ecc.h"

#include <esp_sleep.h>
#include <string.h>

typedef enum {
    PUF_SLEEP_IDLE,
    PUF_SLEEP_NEED_RESET,     //Data needs to be fetched from D-SRAM
    PUF_SLEEP_RAW_DATA_READY, //Raw data from D-SRAM are ready
    PUF_SLEEP_RESPONSE_READY, //Final masked and error corrected data
    PUF_SLEEP_RESPONSE_FAIL,  //Error correction failure
} puf_sleep_data_state;

//If we define it here, this variable will be "hidden" from user
RTC_DATA_ATTR puf_sleep_data_state puf_data_state;
RTC_DATA_ATTR size_t puf_measurements;

//Buffer to store RAW D-SRAM data right after reset
__NOINIT_ATTR uint8_t puf_sleep_raw_data[PUF_MEMORY_SIZE];

void pufsleep_bit_frequency_helper() {
    uint16_t *puf_freq;
    const esp_partition_t *puf_partition = get_puf_partition();
    
    const size_t puf_data_size = PUF_MEMORY_SIZE * sizeof(uint16_t);
    if(puf_measurements == 0) { //Initial measurement
        //Partition region needs to be erased before every write
        ESP_ERROR_CHECK(esp_partition_erase_range(puf_partition, 0, puf_data_size));

        puf_freq = calloc(sizeof(uint16_t), PUF_MEMORY_SIZE);
        ESP_ERROR_CHECK(esp_partition_write(puf_partition, 0, puf_freq, puf_data_size));
    }
    else {
        puf_freq = malloc(puf_data_size);
        ESP_ERROR_CHECK(esp_partition_read(puf_partition, 0, puf_freq, puf_data_size));

        for (size_t j = 0; j < PUF_MEMORY_SIZE; ++j) {
            puf_freq[j] += GET_BIT(puf_sleep_raw_data, j);
        }

        //Partition region needs to be erased before every write
        ESP_ERROR_CHECK(esp_partition_erase_range(puf_partition, 0, puf_data_size));
        ESP_ERROR_CHECK(esp_partition_write(puf_partition, 0, puf_freq, puf_data_size));        
        free(puf_freq);
    }

    puf_data_state = PUF_SLEEP_IDLE;
    if(puf_measurements < PROVISIONING_MEASUREMENTS_SLEEP) {
        puf_measurements++;
        //If we have remaining measurements then this function not returns
        get_puf_response_reset();
    } 
}

void get_pufsleep_bit_frequency(uint16_t **puf_freq) {
    //We need to ensure that wake_up_stub was actually called
    if(puf_data_state != PUF_SLEEP_RAW_DATA_READY) {
        puf_measurements = 0;
        get_puf_response_reset();
    }

    pufsleep_bit_frequency_helper();

    const size_t puf_data_size = PUF_MEMORY_SIZE * sizeof(uint16_t);
    *puf_freq = malloc(puf_data_size);
    assert(*puf_freq != NULL);

    const esp_partition_t *puf_partition = get_puf_partition();
    ESP_ERROR_CHECK(esp_partition_read(puf_partition, 0, *puf_freq, puf_data_size));
}

bool get_puf_response_reset() {
    //There is no need to reset if we already have raw PUF
    if(puf_data_state == PUF_SLEEP_RAW_DATA_READY)
        return false;

    puf_data_state = PUF_SLEEP_NEED_RESET;
    esp_sleep_enable_timer_wakeup(PUFSLEEP_RESPONSE_SLEEP_uS);
    esp_deep_sleep_start();

    //There is a possibility that sleep was rejected
    return false;
}

void puf_response_reset_calculate() {
    //We need to have valid RAW SRAM data and initialized ECC and mask
    assert(puf_data_state == PUF_SLEEP_RAW_DATA_READY && puf_provision != NULL);

    // apply mask
    apply_puf_mask((uint32_t *) puf_provision->sleep_mask_data, (uint32_t *) puf_sleep_raw_data);

    // correct the masked response using the ECC data
    const bool correction_success = correct_data((uint64_t *) puf_sleep_raw_data, (uint64_t *) puf_provision->sleep_ecc_data, &PUF_RESPONSE);

    //Securely "erase" raw PUF
    mbedtls_platform_zeroize(puf_sleep_raw_data, PUF_MEMORY_SIZE);

    //ECC failed, PUF response needs to invalidated
    if(!correction_success) {
        clean_puf_response();
        puf_data_state = PUF_SLEEP_RESPONSE_FAIL;
    } else {
        puf_data_state = PUF_SLEEP_RESPONSE_READY;
    }
}

RTC_IRAM_ATTR void puf_sleep_wake_up_stub() {
    //Copy raw data only when necessary
    if(puf_data_state == PUF_SLEEP_NEED_RESET) {
        memcpy(puf_sleep_raw_data, (uint8_t*) DATA_SRAM_MEMORY_ADDRESS, PUF_MEMORY_SIZE);
        puf_data_state = PUF_SLEEP_RAW_DATA_READY;
    }
}

bool puf_sleep_data_ready() {
    return puf_data_state == PUF_SLEEP_RESPONSE_READY;
}

#endif //PUF_ENABLE_DEEP_SLEEP