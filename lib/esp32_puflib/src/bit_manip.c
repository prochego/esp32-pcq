#include "bit_manip.h"

void SET_BIT_arr(uint8_t *value, const size_t position, const bool bit) {
    const uint8_t val = (1 << (position % 8));
    if(bit) {
        value[position / 8] |= val;
    } else {
        value[position / 8] &= ~val;
    }
}

void SET_BIT(uint8_t *value, const uint8_t position, const bool bit) {
    const uint8_t val = (1 << position);
    if(bit) {
        *value |= val;
    } else {
        *value &= ~val;
    }
}

inline bool GET_BIT(const uint8_t *value, const size_t position) {
    return (value[position / 8] >> (position % 8)) & 0x1;
}

inline bool HIGHEST_BIT(const uint8_t value) {
    return GET_BIT(&value, 7);
}