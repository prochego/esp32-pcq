#include "puf_defines.h"

#ifdef PUF_ENABLE_RTC

#include <stdio.h>
#include <string.h>
#include <soc/rtc_cntl_reg.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <math.h>

#include "puf_measurement.h"
#include "bit_manip.h"
#include "ecc.h"

uint8_t * const RTC_FAST_MEMORY = (uint8_t*) RTC_FAST_MEMORY_ADDRESS;

inline void power_down_rtc_sram() {
    CLEAR_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PU | RTC_CNTL_FASTMEM_FORCE_NOISO);
    SET_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PD | RTC_CNTL_FASTMEM_FORCE_ISO);
}

inline void power_up_rtc_sram() {
    CLEAR_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PD | RTC_CNTL_FASTMEM_FORCE_ISO);
    SET_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PU | RTC_CNTL_FASTMEM_FORCE_NOISO);
}

void restore_rtc_sram(uint8_t *backup) {
    memcpy(RTC_FAST_MEMORY, backup, RTC_FASTMEM_SIZE);
    free(backup);
}

uint8_t *backup_rtc_sram() {
    uint8_t *backup = malloc(RTC_FASTMEM_SIZE);

    memcpy(backup, RTC_FAST_MEMORY, RTC_FASTMEM_SIZE);
    return backup;
}

uint32_t calc_delay() {
//If ESP32 supports temperature sensor, we can create RAM temperature-error model
//And from that calculate approximate delay needed for normal PUF function
#ifdef PUF_USE_TEMPERATURE_MODEL
    //TODO: Polynomial regression
    //TODO: Store polynomial coefficients on flash
    float current_temp_c;
    ESP_ERROR_CHECK(temperature_sensor_get_celsius(temp_handle, &current_temp_c));

    return X4_COEFF * powf(current_temp_c, 4) + X3_COEFF * powf(current_temp_c, 3) + X2_COEFF * powf(current_temp_c, 2) + X1_COEFF * current_temp_c + C_COEFF;
#else
    //else we fall back to constant delay version
    return PUF_RESPONSE_SLEEP_uS;
#endif
}

void power_cycle_rtc_ram() {
    power_down_rtc_sram();
    esp_rom_delay_us(calc_delay()); // busy loop
    power_up_rtc_sram();
}

void get_puf_bit_frequency(uint16_t **puf_freq) {
    assert(puf_freq != NULL);

    uint16_t *bit_freq = calloc(sizeof(uint16_t), PUF_MEMORY_SIZE * 8);
    uint8_t *backup = backup_rtc_sram();
    
    for (size_t i = 0; i < PROVISIONING_MEASUREMENTS_RTC; ++i) {
        power_cycle_rtc_ram();
        
        for (size_t j = 0; j < PUF_MEMORY_SIZE * 8; ++j) {
            bit_freq[j] += GET_BIT(RTC_FAST_MEMORY, j);
        }

        //Because of task watchdog we must yield in each measurement
        vTaskDelay(1);
    }
    
    restore_rtc_sram(backup);
    *puf_freq = bit_freq;
}

bool get_puf_response() {
    //ECC and mask data need to mapped at this point
    assert(puf_provision != NULL);

    uint8_t *backup = backup_rtc_sram();

    // measure PUF response and apply the mask
    power_cycle_rtc_ram();

    apply_puf_mask((uint32_t*) puf_provision->rtc_mask_data, (uint32_t*) RTC_FAST_MEMORY);

    const bool correct_success = correct_data((uint64_t*) RTC_FAST_MEMORY, (uint64_t*) puf_provision->rtc_ecc_data, &PUF_RESPONSE);

    restore_rtc_sram(backup);

    return correct_success;
}

bool get_puf_response_temp_model(const size_t delay) {
    assert(puf_provision != NULL);

    uint8_t *backup = backup_rtc_sram();

    memset(RTC_FAST_MEMORY, 0xFF, PUF_MEMORY_SIZE);
    
    power_down_rtc_sram();
    esp_rom_delay_us(delay);
    power_up_rtc_sram();

    apply_puf_mask((uint32_t*) puf_provision->rtc_mask_data, (uint32_t*) RTC_FAST_MEMORY);

    const bool correct_success = correct_data((uint64_t*) RTC_FAST_MEMORY, (uint64_t*) puf_provision->rtc_ecc_data, &PUF_RESPONSE);

    restore_rtc_sram(backup);

    return correct_success;
}

#endif //PUF_ENABLE_RTC