//
// Original author:
//      Ondrej Stanicek
//      staniond@fit.cvut.cz
//      2022
// Modified by:
//      Egon Procházka
//      prochego@fit.cvut.cz
//      2024
// Czech Technical University - Faculty of Information Technology
//
#include <string.h>
#include <math.h>
#include <esp_log.h>
#include <esp_random.h>

#include "ecc.h"
#include "bit_manip.h"
#include "puf_measurement.h"
#include "puf_defines.h"
#include "puf_integrity.h"

/**
 * Array of precalculated outputs of the majority_bit function for all of the possible bytes
 */
const uint8_t majority_table[256] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
        0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
};

const uint8_t hw_table[256] = {
        0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
        1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8,
};

int32_t random_from_interval(const int32_t min, const int32_t max) {
    const uint32_t range = max - min + 1;
    const uint32_t chunkSize = UINT32_MAX / range; 
    const uint32_t endOfLastChunk = chunkSize * range;

    uint32_t rand = esp_random();
    while(rand >= endOfLastChunk) {
        rand = esp_random();
    }

    return min + rand / chunkSize;
}

/**
 * Generates the ECC data for 8x repetition code from the PUF reference response.
 * @param puf_reference PUF reference response from which the ECC data is calculated
 * @param ecc_data resulting ECC data will be saved to this array
 */
void generate_ecc_data(const uint8_t *puf_reference, uint8_t *ecc_data) {
    size_t bit_bias[2];
    uint8_t checked_bit_pos = 0, correction_needed = 1, bit_selected = 0;
    int32_t correction = 0;

    while(correction_needed) {
        bit_bias[0] = bit_bias[1] = 0;
        
        for (size_t i = 0; i < PUF_MEMORY_SIZE; ++i) {
            while(checked_bit_pos != 255) {
                const uint8_t bit_pos = random_from_interval(0, 7);
                if(GET_BIT(&checked_bit_pos, bit_pos) == 1) {
                    continue;
                }

                SET_BIT(&checked_bit_pos, bit_pos, 1);
                if(GET_BIT(puf_reference, i * 8 + bit_pos)) {
                    if(bit_bias[1] > bit_bias[0] + correction + random_from_interval(-127, 128)) {
                      continue;
                    }

                    ecc_data[i] = ~(puf_reference[i]);
                    bit_bias[1]++;
                    bit_selected = 1;
                    break;
                } else {
                    if(bit_bias[0] > bit_bias[1] + -correction + random_from_interval(-127, 128)) {
                      continue;
                    }

                    ecc_data[i] = puf_reference[i];
                    bit_bias[0]++;
                    bit_selected = 1;
                    break;
                }
            }

            if(!bit_selected) {
                const uint8_t bit_pos = random_from_interval(0, 7);
                if(GET_BIT(puf_reference, i * 8 + bit_pos)) {
                    ecc_data[i] = ~(puf_reference[i]);
                    bit_bias[1]++;
                } else {
                    ecc_data[i] = puf_reference[i];
                    bit_bias[0]++;
                }
            }

            bit_selected = 0;
            checked_bit_pos = 0;
        }

        //Check if the bias of a corrected response is within the limits
        correction_needed = bit_bias[0] != bit_bias[1];
        correction += bit_bias[0] - bit_bias[1];
    }

    ESP_LOGD(PUF_TAG, "Bit bias: %d - %d", bit_bias[0], bit_bias[1]);
    ESP_LOGD(PUF_TAG, "Correction coeff: %ld", correction);
}

/**
 * Generates the ECC data for 8x repetition code from the PUF reference response.
 * The ECC data will be generated according to the template in such a way, that the resulting PUF response will be the
 * same for \p puf_reference and for \p template_reference after using ECC on \p puf_reference.
 * @param puf_reference PUF reference response from which the ECC data is calculated
 * @param template_reference another PUF reference that will be used to calculate the ECC data
 * @param ecc_data resulting ECC data will be save/debugd to this array
 * @param len length of the \p puf_reference, \p template_reference and \p ecc_data in bytes (their length needs to be the same)
 */
void generate_ecc_data_template(const uint8_t* puf_reference, const uint8_t *template_reference,
                                uint8_t ecc_data[PUF_MEMORY_SIZE]) {
    //Input parameters check
    assert(puf_reference != NULL && template_reference != NULL && ecc_data != NULL);

    for(size_t i = 0; i < PUF_MEMORY_SIZE; ++i) {
        const uint8_t bit_pos = random_from_interval(0, 7);

        if(GET_BIT(template_reference, i * 8 + bit_pos)) {
            ecc_data[i] = ~(puf_reference[i]);
        } else {
            ecc_data[i] = puf_reference[i];
        }
    }
}

/**
 * Creates a mask of stable PUF bits from a frequency array.
 * @param puf_freq a frequency array of the PUF response
 * @param mask the resulting mask will be saved to this array - array will be size of ``PUF_MEMORY_SIZE``
 * @param mask_hw out param to which the mask hamming weight will be saved (the number of 1 bits of the mask)
 * @param measurements how many PUF responses have been measured
 */
void create_puf_mask(const uint16_t *puf_freq, uint8_t mask[PUF_MEMORY_SIZE], size_t* mask_hw, const size_t measurements) {
    //Check input parameters
    assert(puf_freq != NULL && mask != NULL && mask_hw != NULL);

    const uint32_t mask_upper_bound = roundf(measurements * (1 - STABLE_BIT_PROBABILITY));
    const uint32_t mask_lower_bound = roundf(measurements *      STABLE_BIT_PROBABILITY);

    *mask_hw = 0;
    for(size_t i = 0; i < PUF_MEMORY_SIZE * 8; ++i) {
        const bool bit = puf_freq[i] >= mask_upper_bound || puf_freq[i] <= mask_lower_bound;
        *mask_hw += bit;
        
        SET_BIT_arr(mask, i, bit);
    }
}

void apply_puf_mask(const uint32_t *mask, uint32_t *puf_response) {    
    for(size_t i = 0; i < PUF_MEMORY_SIZE / 4; i++)
        puf_response[i] &= mask[i];
}

int correct_data(const uint64_t *puf_response, const uint64_t *ecc_data, uint8_t **result) {    
    uint8_t *corrected_response = malloc(PUF_MEMORY_SIZE / 8);

    uint64_t code_d_qword;
    uint8_t *code_bytes = (uint8_t *) &code_d_qword, tmp;
    size_t error_count = 0;
    
    for(size_t i = 0; i < PUF_MEMORY_SIZE / 8; i++) {
        code_d_qword = puf_response[i] ^ ecc_data[i];

        corrected_response[i] = majority_table[code_bytes[7]] << 7 | majority_table[code_bytes[6]] << 6 
                              | majority_table[code_bytes[5]] << 5 | majority_table[code_bytes[4]] << 4 
                              | majority_table[code_bytes[3]] << 3 | majority_table[code_bytes[2]] << 2 
                              | majority_table[code_bytes[1]] << 1 | majority_table[code_bytes[0]];

        for(uint8_t k = 0; k < 8; k++) {
            tmp = code_bytes[k];
            if(majority_table[tmp])
                tmp = ~tmp;
   
            error_count += hw_table[tmp];
        }
    }

    //printf("Error count: %d\n", error_count);
    if(unlikely(error_count > PUF_ERROR_THRESHOLD)) {
        free(corrected_response);
        *result = NULL;
        return false;
    }

    *result = corrected_response;
    return true;
}

/**
 * @brief Creates a PUF reference response from the frequency array.
 * @param puf_freq a frequency array of the PUF response - array of size ``PUF_MEMORY_SIZE * 8``
 * @param puf_reference an array to which the resulting PUF reference is saved
 * @param measurements how many PUF responses have been measured
 */
void create_puf_reference(const uint16_t *puf_freq, uint8_t **puf_reference, const size_t measurements) {
    uint8_t *final_reference = (uint8_t *) malloc(PUF_MEMORY_SIZE);

    for(size_t i = 0; i < PUF_MEMORY_SIZE * 8; i++) {
        // bit is 0 in the reference if it is 0 in more than half of the PUF measurements
        const bool bit = puf_freq[i] > (measurements / 2);
        SET_BIT_arr(final_reference, i, bit);
    }

    *puf_reference = final_reference;
}

/**
 * @brief Used to create masked PUF reference response
 * @param masked_reference array that will contain masked reference, array is allocated by function
 * @param puf_frequency measured frequency of each bit
 * @param mask mask created by ``create_puf_mask`` function
 * @param measurements How many PUF responses have been measured
 * @note  Resulting array will be length of stable_bit_count / 8
*/
void get_masked_reference(uint8_t **masked_reference, const uint16_t *puf_frequency, 
                          const uint8_t *mask, const size_t measurements) {
    uint8_t *puf_reference;
    create_puf_reference(puf_frequency, &puf_reference, measurements);
    apply_puf_mask((uint32_t*) mask, (uint32_t*) puf_reference);
    *masked_reference = puf_reference;
}

void provision_puf_helper(const uint16_t* puf_freq_rtc, const uint16_t* puf_freq_sleep) {
    puf_provision_data puf_provision;

    puf_provision.begin_delim = 0xAF;
    puf_provision.end_delim = 0xFA;

//Enable mask creation only if enabled in config
#ifdef PUF_ENABLE_RTC
    ESP_LOGD(PUF_TAG, "Creating RTC stable bit mask");
    size_t mask_rtc_hw;
    create_puf_mask(puf_freq_rtc, puf_provision.rtc_mask_data, &mask_rtc_hw, PROVISIONING_MEASUREMENTS_RTC);    
    ESP_LOGD(PUF_TAG, "RTC SRAM stable bits %d", mask_rtc_hw);
    
    //Check for stable bit count
    //Number of absolute minimum stable bits is based on user needs
    if(unlikely(mask_rtc_hw < PUF_STABLE_BIT_THRESHOLD)) {
        ESP_LOGE(PUF_TAG, "Low RTC memory stable bit count! Provisioning aborted");
        return;
    }
#endif

#ifdef PUF_ENABLE_DEEP_SLEEP
    size_t mask_sleep_hw;
    create_puf_mask(puf_freq_sleep, puf_provision.sleep_mask_data, &mask_sleep_hw, PROVISIONING_MEASUREMENTS_SLEEP);
    ESP_LOGD(PUF_TAG, "Data SRAM stable bits %d", mask_sleep_hw);
    if(unlikely(mask_sleep_hw < PUF_STABLE_BIT_THRESHOLD)) {
        ESP_LOGE(PUF_TAG, "Low deep sleep memory stable bit count! Provisioning aborted");
        return;
    }
#endif    
    
    //Get partition to store all provision data
    const esp_partition_t *puf_partition = get_puf_partition();

    //We need to check if all provision data will fit into the partition
    if(unlikely(sizeof(puf_provision_data) > puf_partition->size)) {
        ESP_LOGE(PUF_TAG, "PUF partition is too small to fit all provision data!");
        return;
    }

    ESP_ERROR_CHECK(esp_partition_erase_range(puf_partition, 0, PUF_PARTITION_SIZE));

//RTC mask write and reference creation
#ifdef PUF_ENABLE_RTC
    ESP_LOGD(PUF_TAG, "Creating masked reference RTC");
    uint8_t *masked_reference_rtc;
    get_masked_reference(&masked_reference_rtc, puf_freq_rtc, puf_provision.rtc_mask_data, PROVISIONING_MEASUREMENTS_RTC);
#endif

//Sleep provision creation
#ifdef PUF_ENABLE_DEEP_SLEEP
    //Calculate masked reference sleep data
    uint8_t *masked_reference_sleep;
    get_masked_reference(&masked_reference_sleep, puf_freq_sleep, puf_provision.sleep_mask_data, PROVISIONING_MEASUREMENTS_SLEEP);

    //Generate sleep ECC data
    generate_ecc_data(masked_reference_sleep, puf_provision.sleep_ecc_data);    
#endif

//RTC ECC generation part
#ifdef PUF_ENABLE_RTC
//In this segment we need to change RTC ECC data generation based on config
//If sleep and RTC PUF is enabled RTC ECC data are generated based on RTC and sleep reference.
//ECC will produce same output for both RTC and sleep PUF
#ifdef PUF_ENABLE_DEEP_SLEEP
    generate_ecc_data_template(masked_reference_rtc, masked_reference_sleep, puf_provision.rtc_ecc_data);
    free(masked_reference_sleep);
#else
    ESP_LOGD(PUF_TAG, "Creating RTC ECC data");
    generate_ecc_data(masked_reference_rtc, puf_provision.rtc_ecc_data);
#endif //PUF_ENABLE_DEEP_SLEEP

    free(masked_reference_rtc);
#endif //PUF_ENABLE_RTC

    //Calculate HMAC of whole provision
    ESP_LOGD(PUF_TAG, "Creating HMAC of provision data");
    calculate_puf_provision_hmac(&puf_provision, puf_provision.provision_data_hmac);

    //Write all PUF provision data to flash 
    ESP_LOGD(PUF_TAG, "Writing PUF provision data to flash");
    ESP_ERROR_CHECK(esp_partition_write(puf_partition, 0, (void*) &puf_provision, sizeof(puf_provision_data)));    
}

void provision_puf_calculate() {
    uint16_t *puf_freq_sleep = NULL, *puf_freq_rtc = NULL;

//Sleep version frequency calculation
#ifdef PUF_ENABLE_DEEP_SLEEP
    get_pufsleep_bit_frequency(&puf_freq_sleep);
#endif

//RTC version frequency calculation
#ifdef PUF_ENABLE_RTC
    ESP_LOGD(PUF_TAG, "Measuring RTC PUF frequency");
    get_puf_bit_frequency(&puf_freq_rtc);
#endif

    ESP_LOGD(PUF_TAG, "Provisioning PUF");
    provision_puf_helper(puf_freq_rtc, puf_freq_sleep);

#ifdef PUF_ENABLE_DEEP_SLEEP
    free(puf_freq_sleep);
#endif

#ifdef PUF_ENABLE_RTC
    free(puf_freq_rtc);
#endif
}

inline void enroll_puf() {
    provision_puf_calculate();
}