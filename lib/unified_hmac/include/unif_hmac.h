#ifndef ESP_UNIF_HMAC_H
#define ESP_UNIF_HMAC_H

#include <stdlib.h>
#include <stdbool.h>

#define HMAC_LEN 32
#define HMAC_KEY_LEN 32

#ifdef __cplusplus
extern "C" {
#endif

void calculate_hmac(const uint8_t *data, const size_t data_len, uint8_t *out_data);

bool check_hmac(const uint8_t *data, const size_t data_len, const uint8_t *hmac);

#ifdef __cplusplus
}
#endif

#endif