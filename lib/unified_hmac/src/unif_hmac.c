#include "unif_hmac.h"
#include <soc/soc_caps.h>
#include <esp_log.h>

#ifdef SOC_HMAC_SUPPORTED
#include <esp_hmac.h>
#define USED_HMAC_KEY HMAC_KEY0
#else
#include <nvs.h>
#include <mbedtls/md.h>
#include <esp_random.h>
#define NVS_HMAC_KEY "hmac_key"
#endif

const char *hmac_tag = "HMAC";

bool constant_time_cmp(const uint8_t *a, const uint8_t *b, const size_t len) {
    uint8_t r = 0;

    for(size_t i = 0; i < len; i++)
        r |= a[i] ^ b[i];

    return (-(uint64_t)r) >> 63;
}

#ifndef SOC_HMAC_SUPPORTED

void generate_hmac_key() {
    nvs_handle_t nvs_handle;
    ESP_ERROR_CHECK(nvs_open("nvs", NVS_READWRITE, &nvs_handle));

    //Erase old key if present
    nvs_erase_key(nvs_handle, NVS_HMAC_KEY);

    //Generate HMAC key using HW RNG
    uint8_t hmac_key[HMAC_KEY_LEN];
    esp_fill_random(hmac_key, HMAC_KEY_LEN);

    ESP_ERROR_CHECK(nvs_set_blob(nvs_handle, NVS_HMAC_KEY, hmac_key, HMAC_KEY_LEN));
    ESP_ERROR_CHECK(nvs_commit(nvs_handle));
    nvs_close(nvs_handle);
}

/**
 * @brief Used to load HMAC key from NVS partition when software HMAC implementation is used
 * @param out_key array of atleast ``PUF_INTEGRITY_KEY_LEN`` bytes where key will be stored
 * @attention NVS partition should be encrypted, otherwise there is no point to use HMAC.
*/
void load_hmac_key(uint8_t out_key[HMAC_KEY_LEN]) {
    nvs_handle_t nvs_handle;
    ESP_ERROR_CHECK(nvs_open("nvs", NVS_READWRITE, &nvs_handle));

    size_t key_len = HMAC_KEY_LEN;
    esp_err_t nvs_err = nvs_get_blob(nvs_handle, NVS_HMAC_KEY, out_key, &key_len);
    
    //Key is not present
    if(nvs_err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGD(hmac_tag, "Generating HMAC key...");
        generate_hmac_key();

        //Try again
        key_len = HMAC_KEY_LEN;
        ESP_ERROR_CHECK(nvs_get_blob(nvs_handle, NVS_HMAC_KEY, out_key, &key_len));
    } else {
        //Other NVS related error
        ESP_ERROR_CHECK(nvs_err);
    }

    nvs_close(nvs_handle);
}

#endif

void calculate_hmac(const uint8_t *data, const size_t data_len, uint8_t *out_data) {
#ifdef SOC_HMAC_SUPPORTED
    //HMAC key needs to be burned first
    ESP_ERROR_CHECK(esp_hmac_calculate(USED_HMAC_KEY, data, data_len, out_data));
#else
    uint8_t hmac_key[HMAC_KEY_LEN];
    load_hmac_key(hmac_key);
    assert(mbedtls_md_hmac(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), hmac_key, HMAC_KEY_LEN, data, data_len, out_data) == 0);
#endif
}

bool check_hmac(const uint8_t *data, const size_t data_len, const uint8_t *original_hmac) {
    uint8_t calculated_hmac[HMAC_LEN];    
    calculate_hmac(data, data_len, calculated_hmac);

    return constant_time_cmp(calculated_hmac, original_hmac, HMAC_LEN) == 0;
}