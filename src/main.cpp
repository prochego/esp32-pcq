#include <kyber_puf.hpp>
#include <kem.h>
#include <esp_log.h>
#include <nvs_flash.h>
#include <bootloader_random.h>
#include <unif_hmac.h>

static const char *TAG = "KyberPUF example";

extern "C" {
    void app_main();
}

void app_main() {
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    ESP_ERROR_CHECK(err);

    //Use only if RF subsystem is not enabled
    bootloader_random_enable();
    
    //Initializes PUF library and KyberPUF memory mapping
    init_kyberpuf();
    
    uint8_t *public_key = new uint8_t[KYBER_PUBLICKEYBYTES];
    uint8_t *private_key = new uint8_t[KYBER_SECRETKEYBYTES];

    if(!kyber_generate_keypair(public_key, private_key)) {
        ESP_LOGE(TAG, "Keypair generation failed");
        return;
    }

    uint8_t *ciphertext = new uint8_t[KYBER_CIPHERTEXTBYTES];
    uint8_t ss[KYBER_SSBYTES], ss1[KYBER_SSBYTES];

    //Ciphertext and shared secret from given public key
    crypto_kem_enc(ciphertext, ss, public_key);

    //Generate shared secret from received ciphertext and private key
    crypto_kem_dec(ss1, ciphertext, private_key);

    delete[] public_key;
    delete[] private_key;
    delete[] ciphertext;

    //Deinitialize PUF library and KyberPUF memory mapping
    deinit_kyberpuf();
}